# Overview

This repository contains my graduation project (Biotechnology Engineer).
It is related to the characterization of the Sherp Protein.

All Details will be available in my report that can be found here: [graduation papers](https://drive.google.com/open?id=0B_WVYcnB9tOCY0YtTkF0aEs0Tlk).

Through out my project, I used the [modeller]() framework. 
Make sure to download it before going through the below steps.

My project is using Shell and Python scripts.

# Get the code

Clone the repository, move into the repository 

	hg clone https://sara_nzh@bitbucket.org/sara_nzh/sherp_protein_project
	cd sherp_protein_project

# Generate the results

To generate the results, run one of the below command

* `mod9.14 run.py`
* or `python run.py`

The results will be available in the folder `results`.

# Clean

To clean the project and remove the generated results, run the below command

	./clean

# Folder structure

Below you'll find the details of the folders available in this repository

* `inputs`: contains our protein sequence
* `templates`: contains few templates
* `results`: contains the output of the run script