from modeller import *
from modeller.automodel import *
from modeller.scripts import complete_pdb
import os
import pylab


##################
## CONFIGURATION
##################

# Set log level
log.none()

# The folder containing the sequence
RESULTS_FOLDER = 'results/'

# The results folder
INPUT_FOLDER = 'inputs/'

# The folder containing templates
TEMPLATE_FOLDER = 'templates/'

# This is a dictionary containing the name of the templates and a list of the according chains used
MY_TEMPLATES = {
    '2X43':['S'],
    '1JS8':['A', 'B'],
    '2HSN':['A'],
    '3GEU':['A','B','C','D'],
    '4BTP':['A','B','C','D','E','F','G','H','I','J'],
    '4BX4':['A','B'],
    '2LLK':['A']
}

# If directory "results" does not exist, create it
if not os.path.exists(RESULTS_FOLDER):
    os.makedirs(RESULTS_FOLDER)

# If directory "inputs" does not exist, print message and exit with error
if not os.path.exists(INPUT_FOLDER):
    print "I could not find the input directory"
    os._exit(1)


##################
## FUNCTIONS
##################

# Copied the below functions from the Manuel

def r_enumerate(seq):
    """Enumerate a sequence in reverse order"""
    # Note that we don't use reversed() since Python 2.3 doesn't have it
    num = len(seq) - 1
    while num >= 0:
        yield num, seq[num]
        num -= 1

def get_profile(profile_file, seq):
    """Read `profile_file` into a Python array, and add gaps corresponding to
       the alignment sequence `seq`."""
    # Read all non-comment and non-blank lines from the file:
    f = file(profile_file)
    vals = []
    for line in f:
        if not line.startswith('#') and len(line) > 10:
            spl = line.split()
            vals.append(float(spl[-1]))
    # Insert gaps into the profile corresponding to those in seq:
    for n, res in r_enumerate(seq.residues):
        for gap in range(res.get_leading_gaps()):
            vals.insert(n, None)
    # Add a gap at position '0', so that we effectively count from 1:
    vals.insert(0, None)
    return vals


# This function will create an allignment between a template and our sequence.
# It will create the resuled 2 files in the directory "results" using as a prefix the template name.
def s_align(template, chain):
    # create a new MODELLER environment to build this model in
    env = environ()

    # directories for input atom files
    env.io.atom_files_directory = ['.', TEMPLATE_FOLDER]

    # create an allignment list
    aln = alignment(env)

    # Read in HETATM records from template PDBs
    env.io.hetatm = True

    # needed to generate the profiles
    env.libs.topology.read(file='$(LIB)/top_heav.lib') 
    env.libs.parameters.read(file='$(LIB)/par.lib') 

    # Model from template
    mdl = model(env, file=template+'.pdb', model_segment=('FIRST:'+chain, 'LAST:'+chain))
    aln.append_model( mdl, atom_files=template+'.pdb', align_codes=template)

    # Add my sequence
    aln.append(file=INPUT_FOLDER + 'Mex.ali', align_codes='Mex')

    # Do allignment between my sequence and the template    
    aln.salign(output='ALIGNMENT')

    # Create results
    aln.write(file=RESULTS_FOLDER + template + '-' + chain + '-alligned.ali', alignment_format='PIR')
    aln.write(file=RESULTS_FOLDER + template + '-' + chain + '-alligned.pap', alignment_format='PAP')

    # Generate profile needed for chart
    mdl2 = complete_pdb(env, TEMPLATE_FOLDER + template + '.pdb')

    s = selection(mdl2)
    s.assess_dope(output='ENERGY_PROFILE NO_REPORT', file=RESULTS_FOLDER + template + '-' + chain + '-template.profile',
                  normalize_profile=True, smoothing_window=15)


# This function will create the model of every alligned template
def create_model(template, chain):
    # create a new MODELLER environment to build this model in
    env = environ()

    # directories for input atom files
    env.io.atom_files_directory = ['.', TEMPLATE_FOLDER]

    # Read in HETATM records from template PDBs
    env.io.hetatm = True

    # needed to generate the profiles
    env.libs.topology.read(file='$(LIB)/top_heav.lib') 
    env.libs.parameters.read(file='$(LIB)/par.lib') 

    a = automodel(
        env, 
        alnfile=RESULTS_FOLDER + template + '-' + chain + '-alligned.ali',
        knowns=(template),
        sequence='Mex',
        assess_methods = (assess.DOPE, assess.GA341)
    )

    # create one model (my sequence and the aligned template)
    a.starting_model = 1
    a.ending_model = 1
    a.make()

    for m in a.outputs :
        model_name = m['name']

        # Create profile needed to generate chart
        mdl = complete_pdb(env, model_name)
        s = selection(mdl)
        s.assess_dope(output='ENERGY_PROFILE NO_REPORT', file=RESULTS_FOLDER + template + '-' + chain + '-model.profile',
                      normalize_profile=True, smoothing_window=15)

        # move the ouput model to the directory "results"
        os.rename(
            model_name,
            RESULTS_FOLDER + model_name + '-' + template + '-' + chain + '.pdb')


# This function will generate a chart for every template and chain.
def generate_chart(template, chain):
    # create a new MODELLER environment to build this model in
    env = environ()

    # directories for input atom files
    env.io.atom_files_directory = ['.', TEMPLATE_FOLDER]

    # Read in HETATM records from template PDBs
    env.io.hetatm = True

    # needed to generate the profiles
    env.libs.topology.read(file='$(LIB)/top_heav.lib') 
    env.libs.parameters.read(file='$(LIB)/par.lib') 

    # Take the result of the allignment
    a = alignment(env, file=RESULTS_FOLDER + template + '-' + chain + '-alligned.ali')

    # Take the profile created after allignment
    templ = get_profile(RESULTS_FOLDER + template + '-' + chain + '-template.profile', a[template])

    # Take the profile created after model
    model = get_profile(RESULTS_FOLDER + template + '-' + chain + '-model.profile'   , a['Mex'])

    # Plot the template and model profiles in the same plot for comparison:
    pylab.figure(template + '-' + chain, figsize=(15,10))
    pylab.xlabel('Alignment position')
    pylab.ylabel('DOPE per-residue score')
    pylab.plot(model, color='red', linewidth=2, label='Model')
    pylab.plot(templ, color='green', linewidth=2, label='Template ' + template + ':' + chain)
    pylab.legend()
    pylab.savefig(RESULTS_FOLDER + 'dope_profile-' + template + '-' + chain + '.png', dpi=65)



##################
## MAIN
##################

# Create allignment of every template, then used the ouput to create the model
for template_name in MY_TEMPLATES:
    chains = MY_TEMPLATES[template_name]
    for chain in chains:
        print 'Allign "' + template_name + ':' + chain +  '" with the sequence.'
        s_align(template_name, chain)

        print 'Create model for:  "' + template_name + ':' + chain +  '".'
        create_model(template_name, chain)

        print 'Generate chart for: "' + template_name + ':' + chain +  '".'
        generate_chart(template_name, chain)
